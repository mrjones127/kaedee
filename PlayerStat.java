package none.alchemy.alchemyskaedee;

public class PlayerStat{
	public String name;
	public int kills, deaths, killstreak, killstreakAlltime;
	public short killsListPos = -1, KDRListPos = -1, killstreakListPos = -1, alltimeKillstreakListPos = -1;
	public PlayerStat(String name, int kills, int deaths, int killstreak, int killstreakAlltime) {
		this.name = name;
		this.kills = kills;
		this.deaths = deaths;
		this.killstreak = killstreak;
		this.killstreakAlltime = killstreakAlltime;
	}
}