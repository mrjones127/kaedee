package none.alchemy.alchemyskaedee;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

public final class AlchemysKaeDee extends JavaPlugin {
	int updateFrequencyInTicks = 20;
	@Override
	public void onEnable() {
		// Plugin startup logic. plugin wont load without papi so whatever
		saveDefaultConfig();
		FileConfiguration config = getConfig();
		this.updateFrequencyInTicks = config.getInt("updatefrequencyinticks");
		MySQLHelper.topListTakeAmount = config.getInt("toplisttakeamount");
		MySQLHelper.host = config.getString("mysql.host");
		MySQLHelper.port = config.getInt("mysql.port");
		MySQLHelper.database = config.getString("mysql.database");
		MySQLHelper.username = config.getString("mysql.username");
		MySQLHelper.password = config.getString("mysql.password");

		//open connection and cache stats
		getServer().getScheduler().runTask(this, () -> {
			try {
				MySQLHelper.openConnection();
				MySQLHelper.cacheStats();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}

			//afterwards start committing updates
			this.getServer().getScheduler().scheduleAsyncRepeatingTask(this, () -> {
				try {
					MySQLHelper.commitStatUpdates();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}, 0, 20);
		});

		if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null){
			new PapiExpansion(this).register();
		}
		new KaeDeeMVdW().registerPlaceholders(this);
		getServer().getPluginManager().registerEvents(new KillListener(this), this);
		this.getCommand("kdr").setExecutor(new GetKaeDeeCommand());
		this.getCommand("kdrinc").setExecutor(new IncrementKillCommand());
	}

	@Override
	public void onDisable() {
		// Plugin shutdown logic
	}
}
