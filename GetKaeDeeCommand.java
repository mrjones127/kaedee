package none.alchemy.alchemyskaedee;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GetKaeDeeCommand implements CommandExecutor {

	private String blankHeader = "====================================";
	int halfHeader = blankHeader.length()/2;
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player))
			return false;
		Player player = (Player)sender;
		PlayerStat stats;
		String name;
		if (args.length > 0){
			OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
			stats = MySQLHelper.playerStatList.get(offlinePlayer.getUniqueId());
			name = offlinePlayer.getName();
		}
		else{
			stats = MySQLHelper.playerStatList.get(player.getUniqueId());
			name = player.getName();
		}
		if (stats == null){
			player.sendMessage("Couldn't find player!");
			return false;
		}


		name += "'s KDR";
		int halfName = (int)Math.ceil(name.length() / 2.0);
		StringBuilder header = new StringBuilder().append(blankHeader);
		header.replace(halfHeader - halfName + 1, halfHeader + halfName - 1, name);
 		player.sendMessage(new String[] {
				header.toString(),
				"  Kills: " + stats.kills,
				"  Deaths: " + stats.deaths,
				"  Ratio: " + stats.kills / (float) Math.max(stats.deaths, 1),
				"  Killstreak: " + stats.killstreak,
				"  All-Time Killstreak: " + stats.killstreakAlltime,
				blankHeader,
		});
		return true;
	}

	//always assume base is longer than input
	private String padText(String input, int halfBaseLength){
		return StringUtils.repeat(" ",  halfBaseLength - input.length() / 2) + input;
	}
}
