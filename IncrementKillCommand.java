package none.alchemy.alchemyskaedee;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class IncrementKillCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player))
			return false;
		Player player = (Player)sender;
		if (!player.hasPermission("kaedee.incrementkills"))
			return false;
		MySQLHelper.updatePlayerStat(player.getUniqueId(), true);
		return true;
	}
}
