package none.alchemy.alchemyskaedee;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.Plugin;

public class PapiExpansion extends PlaceholderExpansion {
	private Plugin plugin;

	/**
	 * Since we register the expansion inside our own plugin, we
	 * can simply use this method here to get an instance of our
	 * plugin.
	 *
	 * @param plugin
	 *        The instance of our plugin.
	 */
	public PapiExpansion(Plugin plugin){
		this.plugin = plugin;
	}

	/**
	 * Because this is an internal class,
	 * you must override this method to let PlaceholderAPI know to not unregister your expansion class when
	 * PlaceholderAPI is reloaded
	 *
	 * @return true to persist through reloads
	 */
	@Override
	public boolean persist(){
		return true;
	}

	/**
	 * Because this is a internal class, this check is not needed
	 * and we can simply return {@code true}
	 *
	 * @return Always true since it's an internal class.
	 */
	@Override
	public boolean canRegister(){
		return true;
	}

	/**
	 * The name of the person who created this expansion should go here.
	 * <br>For convienience do we return the author from the plugin.yml
	 *
	 * @return The name of the author as a String.
	 */
	@Override
	public String getAuthor(){
		return "Alchemy_";
	}

	/**
	 * The placeholder identifier should go here.
	 * <br>This is what tells PlaceholderAPI to call our onRequest
	 * method to obtain a value if a placeholder starts with our
	 * identifier.
	 * <br>This must be unique and can not contain % or _
	 *
	 * @return The identifier in {@code %<identifier>_<value>%} as String.
	 */
	@Override
	public String getIdentifier(){
		return "kaedee";
	}

	/**
	 * This is the version of the expansion.
	 * <br>You don't have to use numbers, since it is set as a String.
	 *
	 * For convienience do we return the version from the plugin.yml
	 *
	 * @return The version as a String.
	 */
	@Override
	public String getVersion(){
		return "1";
	}


	@Override
	public String onRequest(OfflinePlayer player, String identifier){
		//gonna guess a big old switch is faster than some parsing
		//could clean this up to use chararray and be fancy there...
		switch (identifier.charAt(1)){
			case 'o':
				int index = identifier.charAt(3) - '0';
				return  MySQLHelper.topKillsList[index].name + " -> " + MySQLHelper.topKillsList[index].kills;
			case 'd':
				index = identifier.charAt(2) - '0';
				return  MySQLHelper.topKDList[index].name + " -> " + String.format("%.02f", MySQLHelper.topKDList[index].kills / (float)Math.max(1, MySQLHelper.topKDList[index].deaths));
			case 's':
				index = identifier.charAt(2) - '0';
				return  MySQLHelper.topKillstreakList[index].name + " -> " + MySQLHelper.topKillstreakList[index].killstreak;
		}
		return "KAEDEEINVALID";
	}
}
