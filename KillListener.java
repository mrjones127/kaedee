package none.alchemy.alchemyskaedee;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import java.sql.SQLException;

public class KillListener implements Listener {
	Plugin thisPlugin;
	public KillListener(Plugin thisPlugin){
		this.thisPlugin = thisPlugin;
	}
	@EventHandler (priority = EventPriority.LOWEST)
	public void onPlayerDeath(PlayerDeathEvent e) {
		Player victim = e.getEntity();
		Player killer = victim.getKiller();
		if (killer == null)
			return;
		MySQLHelper.updatePlayerStat(killer.getUniqueId(), true);
		MySQLHelper.updatePlayerStat(victim.getUniqueId(), false);
	}
	@EventHandler (priority = EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent e){
		try {
			MySQLHelper.ensurePlayerRegistered(e.getPlayer());
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
}
