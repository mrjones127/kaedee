package none.alchemy.alchemyskaedee;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class MySQLHelper {
	public static String host, database, username, password;
	public static int port;
	public static int topListTakeAmount = 10;

	private static Connection connection;
	public static ConcurrentHashMap<UUID, Integer> uuidsToIncrementDeath = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<UUID, Integer> uuidsToIncrementKills = new ConcurrentHashMap<>();
	public static HashSet<UUID> uuidsToUpdateAlltimeKillstreak = new HashSet<>();

	public static PlayerStat[] topKDList = new PlayerStat[topListTakeAmount];
	public static PlayerStat[] topKillstreakList = new PlayerStat[topListTakeAmount];
	public static PlayerStat[] topKillsList = new PlayerStat[topListTakeAmount];
	public static PlayerStat[] topKillstreakAlltimeList = new PlayerStat[topListTakeAmount];

	public static HashMap<UUID, PlayerStat> playerStatList = new HashMap<>();

	public static void ensurePlayerRegistered(Player player) throws SQLException {
		if (playerStatList.get(player.getUniqueId()) != null)
			return;
		playerStatList.put(player.getUniqueId(), new PlayerStat(player.getDisplayName(), 0, 0, 0, 0));
		PreparedStatement prep = connection.prepareStatement("INSERT INTO kaedee_players (uuid, playername, kills, deaths, killstreak, killstreakalltime) VALUES ('" + player.getUniqueId() + "', '" + player.getDisplayName() + "', '0', '0', '0', '0')");
		prep.execute();
		connection.commit();
	}

	public static void resetStats(UUID uuid) throws SQLException {
		connection.prepareStatement("UPDATE kaedee_players SET kills = 0, deaths = 0, killstreak = 0, killstreakalltime = 0 WHERE uuid = '" + uuid + "');").execute();
		connection.commit();
	}

	public static void updatePlayerStat(UUID uuid, boolean killeryesvictimno){
		PlayerStat stat = playerStatList.get(uuid);
		if (killeryesvictimno){
			stat.kills++;
			stat.killstreak++;
			if (MySQLHelper.uuidsToIncrementKills.containsKey(uuid))
				MySQLHelper.uuidsToIncrementKills.put(uuid, MySQLHelper.uuidsToIncrementKills.get(uuid) + 1);
			else MySQLHelper.uuidsToIncrementKills.put(uuid, 1);
			updatePlayerOnKillLeaderboard(stat);

			if (stat.killstreak > stat.killstreakAlltime){
				stat.killstreakAlltime = stat.killstreak;
				uuidsToUpdateAlltimeKillstreak.add(uuid);
				updatePlayerOnKillstreakAlltimeLeaderboard(stat);

				//easter egg
				if (stat.killstreak == 50){
					Player player = Bukkit.getPlayer(uuid);
					if ((player != null)) {
						player.getInventory().addItem(new ItemStack(Material.DIAMOND_BLOCK, 4), new ItemStack(Material.GOLDEN_APPLE, 1, (short) 1));
						player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.BLUE + "KaeDee" + ChatColor.DARK_GRAY + "]" + ChatColor.AQUA + " Congratulations on hitting a killstreak of 50!");
					}
				}
			}
		}
		else{
			playerStatList.get(uuid).deaths++;
			playerStatList.get(uuid).killstreak = 0;
			if (uuidsToIncrementDeath.containsKey(uuid))
				MySQLHelper.uuidsToIncrementDeath.put(uuid, MySQLHelper.uuidsToIncrementDeath.get(uuid) + 1);
			else MySQLHelper.uuidsToIncrementDeath.put(uuid, 1);
			//DO NOT UPDATE KILLSTREAK BOARD HERE ITS EXPENSIVE
			//DO IT WHEN DATABASE CHANGES ARE APPLIED
		}
		updatePlayerOnKDRLeaderboard(stat);
	}


	private static final String deathIncrementerHeader = "UPDATE kaedee_players SET deaths = deaths + 1, killstreak = 0 WHERE uuid IN ('";
	private static final int deathIncrementerHeaderLength = deathIncrementerHeader.length();
	private static final String killIncrementerHeader = "UPDATE kaedee_players SET kills = kills + 1, killstreak = killstreak + 1 WHERE uuid IN ('";
	private static final int killIncrementerHeaderLength = killIncrementerHeader.length();
	private static final String killstreakAlltimeUpdaterHeader = "UPDATE kaedee_players SET killstreakalltime = killstreak WHERE uuid IN ('";
	private static final int killstreakAlltimeUpdaterHeaderLength = killstreakAlltimeUpdaterHeader.length();
	public static void commitStatUpdates() throws SQLException {
		//JANKY
		updateKillstreakLeaderboard();
		//DATABASE UPDATE
		Statement statement = connection.createStatement();
		//because of how UPDATE works, we can only increment kills and deaths. So if someone dies or kills a lot in a short period we have to
		//recache that extra data and let it come around in the queue again
		if (uuidsToIncrementDeath.size() > 0){
			StringBuilder deathIncrementer = new StringBuilder();
			deathIncrementer.append(deathIncrementerHeader);
			for (Iterator<Map.Entry<UUID, Integer>> iter = uuidsToIncrementDeath.entrySet().iterator(); iter.hasNext();){
				Map.Entry<UUID, Integer> entry = iter.next();
				deathIncrementer.append(entry.getKey().toString()).append("', '");
				if (entry.getValue() > 1)
					entry.setValue(entry.getValue() - 1);
				else iter.remove();

			}
			//replace the trailing , ' with );
			deathIncrementer.replace(deathIncrementer.length() - 3, deathIncrementer.length(), ");");
			statement.addBatch(deathIncrementer.toString());
		}
		if (uuidsToIncrementKills.size() > 0){
			StringBuilder killIncrementer = new StringBuilder();
			killIncrementer.append(killIncrementerHeader);
			for (Iterator<Map.Entry<UUID, Integer>> iter = uuidsToIncrementKills.entrySet().iterator(); iter.hasNext();){
				Map.Entry<UUID, Integer> entry = iter.next();
				killIncrementer.append(entry.getKey().toString()).append("', '");
				if (entry.getValue() > 1)
					entry.setValue(entry.getValue() - 1);
				else iter.remove();
			}
			killIncrementer.replace(killIncrementer.length() - 3, killIncrementer.length(), ");");
			statement.addBatch(killIncrementer.toString());
		}

		if (uuidsToUpdateAlltimeKillstreak.size() > 0){
			StringBuilder killstreakAlltimeUpdater = new StringBuilder();
			killstreakAlltimeUpdater.append(killstreakAlltimeUpdaterHeader);
			for (UUID uuid : uuidsToUpdateAlltimeKillstreak)
				killstreakAlltimeUpdater.append(uuid.toString()).append("', '");
			uuidsToUpdateAlltimeKillstreak.clear();
			killstreakAlltimeUpdater.replace(killstreakAlltimeUpdater.length() - 3, killstreakAlltimeUpdater.length(), ");");
			statement.addBatch(killstreakAlltimeUpdater.toString());
		}
		//low chance of nothing happening so just run every time
		statement.executeBatch();
		connection.commit();
	}

	//only run at startup
	public static void cacheStats() throws SQLException {
		PreparedStatement ps = connection.prepareStatement("SELECT * FROM kaedee_players");
		ResultSet rs = ps.executeQuery();
		while (rs.next()){
			playerStatList.put( UUID.fromString(rs.getString("uuid")), new PlayerStat(rs.getString("playername"), rs.getInt("kills"), rs.getInt("deaths"), rs.getInt("killstreak"), rs.getInt("killstreakalltime")));
		}
		rs.close();
		ps.close();
		computeLeaderboards();
	}

	public static void updatePlayerOnKillLeaderboard(PlayerStat stat) {
		//KILLS
		if (stat.killsListPos > -1){
			int index = stat.killsListPos;
			while (index > 0 && stat.kills > topKillsList[index].kills){
				topKillsList[index] = topKillsList[index - 1];
				index--;
			}
			if (stat.kills > topKillsList[index].kills)
				topKillsList[index + 1] = topKillsList[index];
			topKillsList[index] = stat;
		}
		int index = topListTakeAmount - 1;
		if (stat.kills > topKillsList[index].kills) {
			topKillsList[index].killsListPos = -1;
			topKillsList[index] = topKillsList[--index];
			index--;
			while (index > 0 && stat.kills > topKillsList[index].kills) {
				topKillsList[index + 1] = topKillsList[index];
				index--;
			}
			if (stat.kills > topKillsList[index].kills)
				topKillsList[index + 1] = topKillsList[index];
			topKillsList[index] = stat;
			stat.killsListPos = (short)index;
		}
	}

	//fuck
	public static void updateKillstreakLeaderboard(){

//		ArrayList<PlayerStat> stats = new ArrayList<>();
//		Collections.copy(stats, (ArrayList<PlayerStat>)playerStatList.values());
//		 stats.sort((o1, o2) -> {
//			 if (o1.killstreak > o2.killstreak)
//				 return 1;
//			 return 0;
//		 });

		for (PlayerStat stat : playerStatList.values()){
			//KILLSTREAKS
			int index = topListTakeAmount - 1;
			if (stat.killstreak > topKillstreakList[index].killstreak) {
				topKillstreakList[index] = topKillstreakList[--index];
				index--;
				while (index > 0 && stat.killstreak > topKillstreakList[index].killstreak) {
					topKillstreakList[index + 1] = topKillstreakList[index];
					index--;
				}
				if (stat.killstreak > topKillstreakList[index].killstreak)
					topKillstreakList[index + 1] = topKillstreakList[index];
				topKillstreakList[index] = stat;
			}
		}
	}

	public static void updatePlayerOnKillstreakAlltimeLeaderboard(PlayerStat stat){
		//KILLSTREAKS ALLTIME
		int index = topListTakeAmount - 1;
		if (stat.killstreakAlltime > topKillstreakAlltimeList[index].killstreakAlltime) {
			topKillstreakAlltimeList[index] = topKillstreakAlltimeList[--index];
			index--;
			while (index > 0 && stat.killstreakAlltime > topKillstreakAlltimeList[index].killstreakAlltime) {
				topKillstreakAlltimeList[index + 1] = topKillstreakAlltimeList[index];
				index--;
			}
			if (stat.killstreakAlltime > topKillstreakAlltimeList[index].killstreakAlltime)
				topKillstreakAlltimeList[index + 1] = topKillstreakAlltimeList[index];
			topKillstreakAlltimeList[index] = stat;

		}
	}

	public static void updatePlayerOnKDRLeaderboard(PlayerStat stat){
		//KDR
		int index = topListTakeAmount - 1;
		if (stat.kills / (float) Math.max(1, stat.deaths) > topKDList[topListTakeAmount - index].kills / (float) Math.max(1, topKDList[topListTakeAmount - index].deaths)) {
			topKDList[index] = topKDList[--index];
			index--;
			while (index > 0 && stat.kills / (float) Math.max(1, stat.deaths) > topKDList[topListTakeAmount - index].kills / (float) Math.max(1, topKDList[topListTakeAmount - index].deaths)) {
				topKDList[index + 1] = topKDList[index];
				index--;
			}
			if (stat.kills / (float) Math.max(1, stat.deaths) > topKDList[index].kills / (float) Math.max(1, topKDList[index].deaths))
				topKDList[index + 1] = topKDList[index];
			topKDList[index] = stat;
		}
	}

	public static void computeLeaderboards(){
		for (Map.Entry<UUID, PlayerStat> entry : playerStatList.entrySet()){
			PlayerStat tempPlayerStat = entry.getValue();
			updatePlayerOnKillLeaderboard(tempPlayerStat);
			updatePlayerOnKillstreakAlltimeLeaderboard(tempPlayerStat);
			updatePlayerOnKDRLeaderboard(tempPlayerStat);
		}
		updateKillstreakLeaderboard();
	}

	public static void openConnection() throws SQLException, ClassNotFoundException {
		if (connection != null && !connection.isClosed()) {
			return;
		}

		if (connection != null && !connection.isClosed()) {
			return;
		}
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
		connection.setAutoCommit(false);

		//create table if doesnt exist
		if (!connection.getMetaData().getTables(null, null, "kaedee_players", null).next()) {
			connection.prepareStatement("CREATE TABLE kaedee_players(uuid CHAR(36) NOT NULL, playername VARCHAR(16) NOT NULL, kills INT NOT NULL, deaths INT NOT NULL, killstreak INT NOT NULL, killstreakalltime INT NOT NULL, PRIMARY KEY (uuid))").execute();
			connection.commit();
		}

		//add this in here because it needs to be done somewhere and im lazy
		PlayerStat dummyStat = new PlayerStat("Steve", 0, 0, 0, 0);
		for (int i = 0; i < topListTakeAmount; i++){
			topKillsList[i] = dummyStat;
			topKillstreakList[i] = dummyStat;
			topKDList[i] = dummyStat;
			topKillstreakAlltimeList[i] = dummyStat;
		}
	}
}
