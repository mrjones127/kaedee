package none.alchemy.alchemyskaedee;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class KaeDeeMVdW {
	public void registerPlaceholders(Plugin thisPlugin) {
		for (int i = 0; i < 10; i++) {
			String top = "kaedee_top_" + i;
			String kd = "kaedee_kd_" + i;
			String killstreak = "kaedee_killstreak_" + i;
			int finalI = i;
			PlaceholderAPI.registerPlaceholder(thisPlugin, top, placeholderReplaceEvent -> MySQLHelper.topKillsList[finalI].name + " -> " + MySQLHelper.topKillsList[finalI].kills);
			PlaceholderAPI.registerPlaceholder(thisPlugin, kd, placeholderReplaceEvent -> MySQLHelper.topKDList[finalI].name + " -> " + MySQLHelper.topKDList[finalI].kills);
			PlaceholderAPI.registerPlaceholder(thisPlugin, killstreak, placeholderReplaceEvent -> MySQLHelper.topKillstreakList[finalI].name + " -> " + MySQLHelper.topKillsList[finalI].kills);
		}

		PlaceholderAPI.registerPlaceholder(thisPlugin, "kaedee_player_kills", placeholderReplaceEvent -> {
			Player player = placeholderReplaceEvent.getPlayer();
			if (!MySQLHelper.playerStatList.containsKey(player.getUniqueId()))
				return "";
			return Integer.toString(MySQLHelper.playerStatList.get(player.getUniqueId()).kills);
		});

		PlaceholderAPI.registerPlaceholder(thisPlugin, "kaedee_player_deaths", placeholderReplaceEvent -> {
			Player player = placeholderReplaceEvent.getPlayer();
			if (!MySQLHelper.playerStatList.containsKey(player.getUniqueId()))
				return "";
			return Integer.toString(MySQLHelper.playerStatList.get(player.getUniqueId()).deaths);
		});

		PlaceholderAPI.registerPlaceholder(thisPlugin, "kaedee_player_kd", placeholderReplaceEvent -> {
			Player player = placeholderReplaceEvent.getPlayer();
			if (!MySQLHelper.playerStatList.containsKey(player.getUniqueId()))
				return "";
			return String.format("%.02f", (float) MySQLHelper.playerStatList.get(player.getUniqueId()).kills / Math.max( MySQLHelper.playerStatList.get(player.getUniqueId()).deaths, 1));
		});

		PlaceholderAPI.registerPlaceholder(thisPlugin, "kaedee_player_killstreak", placeholderReplaceEvent -> {
			Player player = placeholderReplaceEvent.getPlayer();
			if (!MySQLHelper.playerStatList.containsKey(player.getUniqueId()))
				return "";
			return Integer.toString(MySQLHelper.playerStatList.get(player.getUniqueId()).killstreak);
		});

		PlaceholderAPI.registerPlaceholder(thisPlugin, "kaedee_player_killstreakalltime", placeholderReplaceEvent -> {
			Player player = placeholderReplaceEvent.getPlayer();
			if (!MySQLHelper.playerStatList.containsKey(player.getUniqueId()))
				return "";
			return Integer.toString(MySQLHelper.playerStatList.get(player.getUniqueId()).killstreakAlltime);
		});
	}
}
