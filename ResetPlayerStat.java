package none.alchemy.alchemyskaedee;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.sql.SQLException;

public class ResetPlayerStat implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender).hasPermission("kaedee.resetstats"))
			return false;
		if (args.length < 1){
			sender.sendMessage("Please specify player name");
			return false;
		}
		try {
			MySQLHelper.resetStats(Bukkit.getPlayer(args[0]).getUniqueId());
		} catch (SQLException e) {
			sender.sendMessage("Command failed, did you spell the name correctly?");
			e.printStackTrace();
		}
		return true;
	}
}
